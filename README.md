# kettle-scheduler-boot

#### 介绍
基于Spring-boot的kettle调度项目，参考了[陈zhao](https://gitee.com/uxue)的代码。
主要将项目从Oracle改成了Mysql。其他均保留了原有功能。

#### 更新记录
[2020-07-02]

1，修改kettleConfig的使用方式。原有方式，在linux上进行打包时候，会有几率获取不到配置项；

2，修复首页7日运行状态监控的数据从数据取。




#### 安装教程
1、导入docs中的kettle-scheduler-调度平台脚本.sql文件至Mysql数据库   

#### 使用说明
1.  执行kettle-scheduler-starter下面docs下面的kettle-scheduler-调度平台脚本.sql脚本创建表

2.  生成环境执行时修改application-prod.yml中的数据库连接配置，开发环境修改application-dev.yml中的数据库配置，如果需要修改端口就在application.yml中修改

3.  修改application-kettle.yml配置，设置日志存储路径、kettle脚本保存路径、kettle-home路径（如果没有指定home路径，那么.kettle文件夹就在当前用户根路径下）

4.  如果需要自定义变量在kettle.properties中编写，并把kettle.properties文件拷贝到kettle-home路径下面的.kettle文件夹下

5.  启动项目使用调度平台(用户名：admin，密码：admin)

6.  如果要使用【文件资源库】需要单独把项目下【file-rep】拷贝到设置好的路径下，并在管理页面配置好文件资源库，因为打包后kettle不能访问到jar中的文件，所以需要单独存放

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request