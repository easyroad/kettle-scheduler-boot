package org.kettle.scheduler.system.biz.repository;

import org.kettle.scheduler.system.biz.entity.HomeMonitorTaskCounts;
import org.kettle.scheduler.system.biz.entity.Trans;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author lyf
 */
public interface HomeMonitorTaskCountsRepository extends JpaRepository<HomeMonitorTaskCounts, Integer>, JpaSpecificationExecutor<HomeMonitorTaskCounts> {


}