package org.kettle.scheduler.system.biz.mq.bo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class TransTask {
	
	
	// 分类ID
	private Integer categoryId;
	
	// 转换文件的路径
	private String transPath;
	
	// 转换名称，不能重复。
	private String transName;
	
	// 转换定时策略 1：立即执行一次；2：每周一0点执行一次；3：每月1日0点执行一次；4：每日0点之心过一次
	private Integer transQuartz;
	// 转换参数 json格式
	private String transParams;
	// 同步策略
	private String syncStrategy;
	// 记录日志级别 Error:错误日志；Minimal：最小日志;Basic:基本日志;Detailed:详细日志;Debug:调试;Rowlevel:行级日志(非常详细)
	private String transLogLevel;
	// 转换描述
	private String transDescription;
}
