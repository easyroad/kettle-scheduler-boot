package org.kettle.scheduler.system.biz.component;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

/**
 * 描述:
 *
 * @author xieyonggao
 */
@Component
public class TopicTransSender {

    @Autowired
    RabbitTemplate rabbitTemplate;

    public boolean sendDoneTask(String name,Integer status) {
        //将消息携带绑定键值：kettleDirectRouting kettleDirectExchange
    	JSONObject jsonObject = new JSONObject();
    	jsonObject.put("name", name);
    	jsonObject.put("status", status);
        rabbitTemplate.convertAndSend("kettleTaskExchange", "task.done", JSONObject.toJSONString(jsonObject));
        return true;
    }
}
